public class User {

	private int id;
	private String lastName;
	private double ballance;

	public User(int id, String lastName, double ballance) {
		setID(id);
		setLastName(lastName);
		setBallance(ballance);
	}

	public void setID(int id) {
		if (id > 0) {
			this.id = id;
		} else {
			this.id = 0;
		}
	}

	public int getID() {
		return id;
	}

	public void setLastName(String lastName) {
		if (lastName != "") {
			this.lastName = lastName;
		} else {
			this.lastName = null;
		}
	}

	public String getLastName() {
		return lastName;
	}

	public void setBallance(double ballance) {
		if (ballance > 0) {
			this.ballance = ballance;
		} else {
			System.out.print("You have to assign not correct ballance to ");
			System.out.print(getLastName());		
			System.out.print(" ");
			System.out.println(ballance);
			System.out.println("Error! The balance must be greater than 0! Check it please!");
			System.exit(-1);
		}
	}

	public double getBallance() {
		return ballance;
	}
}