class Program {
	public static void main(String[] args) {
		
		User user1 = new User(1, "John", 1000);
		System.out.print("The ballance of ");
		System.out.print(user1.getLastName());		
		System.out.print(" is ");
		System.out.println(user1.getBallance());

		User user2 = new User(2, "Mike", -500);
		System.out.print("The ballance of ");
		System.out.print(user2.getLastName());		
		System.out.print(" is ");
		System.out.println(user2.getBallance());


		Transaction t1 = new Transaction("1", user1, user2, "INCOME", 100);
		Transaction t2 = new Transaction("1", user2, user1, "OUTCOME", -100);

		System.out.println(t1.getSumma());
		System.out.println(t2.getSumma());

	}
}