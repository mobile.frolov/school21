class Transaction {

	private String uuid;
	private User receiverPayment;
	private User senderPayment;
	private String catTrans;
	private double summa;

	public Transaction(String uuid, User receiverPayment, User senderPayment, String catTrans, double summa) {
		setUUID(uuid);
		setReceiverPayment(receiverPayment);
		setSenderPayment(senderPayment);
		setCatTrans(catTrans);
		setSumma(summa);
	}

	public void	setUUID(String uuid) {
        this.uuid = uuid;
	}

	public String getUUID() {
		return uuid;
	}

	public void	setReceiverPayment(User receiverPayment) {
		this.receiverPayment = receiverPayment;
	}

	public User getReceiverPayment() {
		return receiverPayment;
	}

	public void	setSenderPayment(User senderPayment) {
		this.senderPayment = senderPayment;
	}

	public User getSenderPayment() {
		return senderPayment;
	}

	public void	setCatTrans(String catTrans) {
		this.catTrans = catTrans;
	}

	public String getCatTrans() {
		return catTrans;
	}

    public void	setSumma(double summa) {
    	this.summa = summa;
	}

	public double getSumma() {
		return summa;
	}
}