import java.util.Scanner;

class Program {
	public static void main(String[] args) {
		
		Scanner scanner = new Scanner(System.in);
		int number = scanner.nextInt();
		
		int d = 2; 
		
		if (number < 1) {
			System.err.println("Illegal Argument"); 
		}
		else{
		while (number % d != 0)
		{
		 
			d++;
		}
		if (number == d) {
			System.out.println("true" + " " + (d-1));
		}
		else {
			System.out.println("false" + " " + (d-1));
		}
	}
	}
}