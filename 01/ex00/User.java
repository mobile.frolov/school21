class User {
	private int id;
	private String name;
	private Double balance;

	public User(int id, String name,long balance){
		this.setId = id;
		this.setName = name;
		this.setBalance = balance;
	}
	public void setID(int id){
		if (id > 0){
			this.id = id;
		}
		else {
			this.id = Math.abs(id);
		}
	}
	public int getID(){
		return id;
	}
	public void setName(String name){
		this.name = name;
	}
	public String getName(){
		return name;
	}
	public void setBalance(double balance){
		if (balance < 0){
			this.balance = 0;
		}
		else {
			this.balance =balance;
		}
	}
	public Double getBalance(){
		return balance;
	}
}